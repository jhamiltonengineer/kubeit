#!/usr/bin/env python

"""kubeit.py: wrapper script to simplify telepresence cli."""

__author__      = "Jesse Hamilton"
__version__     = "1.0.0"
__maintainer__  = "Jesse Hamilton"

import sys
import argparse
import os
import logging
import subprocess
import shutil
import urllib2

namespace = 'default'

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument('-d,', action="store", dest='--deploy', help='forge deploy')
parser.add_argument('-p,', action="store", dest='--proxy', help='telepresence')

# Open the docker file and set some stuff up.
dockerfile_lines = open("Dockerfile")
for line in dockerfile_lines:
    if 'EXPOSE' in line:
         Port = line
         Port = Port.strip('EXPOSE')
         Port = Port.lstrip()
         Port = Port.strip(' \t\n\r')
    else:
        if 'deployment=' in line:
             header = line
             header = header.strip('LABEL deployment=')
             header = header.lstrip()
             header = header.replace(" ", "")
             header = header.strip(' \t\n\r')

dockerfile_lines.close()


if (sys.argv[2] == "deploy"):
    subprocess.call(["forge", "--config", "/root/forge.yaml", "deploy"])

if (sys.argv[2] == "proxy"):
   subprocess.call(["docker", "build", "-t", "proxy", "."])
   subprocess.call(["telepresence", "--expose", Port, "--swap-deployment", header, "--namespace", namespace, "--docker-run", "--rm", "-it", "proxy:latest"])

